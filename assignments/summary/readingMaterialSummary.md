<h1>Mindmap of the Summary done on draw.io

![Image of mindmap](https://image.slidesharecdn.com/readingsum-200811090858/95/readingsum-1-638.jpg?cb=1597136959)  
Incase, you can't view the mindmap:
_______________________________________________________________________________________________________________________________________________________
**Challenge for Industries as of now:** Make the shift form Industry 3.0 to 4.0  
______________________________________________________________________________________________________________________________________________________
<h3>Industry 3.0  
Sensors(measures data)-> PLC(collects data)->SCADA and ERP(Store data)->stored in excels and csvs
_Problem identified:_ data do not get plotted as real time graphs or charts

to

<h3>Industry 4.0  
Connecting industry 3.0 to the internet to be able to analyze data reall-time(i/p data immediately processed to give relevant feedback
_Challenge:_ Re aligning industry 3.0 protocols to industry 4.0 protocols 
______________________________________________________________________________________________________________________________________________________

<h2>History of Industrial Revolution

![Industry Revolution of over the years](https://www.thedualarity.com/wp-content/uploads/Industrial-Revolution-4.0-1024x576.png)

1. **Industry 1.0:** Water and steam powered machines were developed to help workers in the mass production of goods  
2. **Industry 2.0:** Transition to mass production methods 
3. **Industry 3.0:** Automation of processes using logic processors and information technology. But, human aspect still present  
   ![What is Industry 3.0](https://image.slidesharecdn.com/malaysianmanufacturinginindustry4-171201004607/95/malaysian-manufacturing-in-industry-40-4-638.jpg?cb=1512089503)  
4.  **Industry 4.0:** Interconnectivity through the Internet of Things (IoT). Gives access to real-time data, and the introduction of cyber-physical systems. Connecting physical with digital and making human aspect almost obsolete.  
![Industry 4.0](https://www.i-scoop.eu/wp-content/uploads/2017/04/From-automation-pyramid-to-industrial-transformation-pyramid-with-Industry-4.jpg.webp)  

_______________________________________________________________________________________________________________________________________________________
<h2>About Industry 4.0

**Advantages:**  
1. Increased Quality as process happens based on the latest relevant information and is optimized  
2. Increase in overall effectiveness of process as it warrants a more flexible response to fluctuations in demand  
3. Better at identifying errors and wastage, thus lowering manufacturing costs  

**Disadvantages:**  
1. Challenges with integrating data  
2. Concerns about data ownership and hence security  
3. increases immediate expenses(but saves cost in the long run)  
4. Downtime due to upgrading  


**Layers of Industry 4.0**  
![Layers of Industry 4.0](https://www.nexcomusa.com/UploadFiles/FCKeditor/image/Applications/2017IAS/NEXCOM_I40_Solution_Map_and_Topology-l.jpg)


**Differences:**  
![Table of Differences](https://www.researchgate.net/profile/Alexander_Zureck/publication/327130483/figure/fig2/AS:769763521347588@1560537333709/Interaction-between-hierarchies-Levels-in-Industry-30-and-Industry-40-source.png)
______________________________________________________________________________________________________________________________________________________

<h2>What exists today?

**Challenges in conversion:**  
1. Expensive Hardwire as licenses for such tools can cost a fortune  
2. Inadequate Documentation, hardware that are installed cannot be fully fathomed by engineers  
3. Trouble with PLC protocols that are proprietary  

**Solution:**  
Several libraries that act as Interfaces on embedded boards that have APIs doing the conversion from data from the PLC and sending it to cloud.  
IoT Gateways are hence much more economically friendly  
![Raspberry Pi is an example](https://www.raspberrypi.org/homepage-9df4b/static/hero-shot-33d83b8c5fa0933373dabcc9462b32a3.png)

**Appurtenances:**  
1. **Plenty of data collected and stored in Time series databases that convert it to beautiful diagrams where you can visualize the data with ease**

![IoT Dashboard](https://blog.thethings.io/wp-content/uploads/2016/02/Screen-Shot-2017-01-23-at-5.08.33-PM-9.png)

2. **IoT Platforms that analyse data such as Azure and AWS, they make heavy server systems and other related hardware obsolete**

![Available Platforms](https://miro.medium.com/max/875/1*go7sTFOGN2fJGgYrI3E-FA.png)

3. **Alerts can be sent to our devices through GPS  with platforms like Twilio**

![How alert system works](https://images.ctfassets.net/2fcg2lkzxw1t/44TF3dmB72cG80MW0qQ0Y6/f9945881f14dcc268f475aedaa210d55/IVR_Diagram.png)

